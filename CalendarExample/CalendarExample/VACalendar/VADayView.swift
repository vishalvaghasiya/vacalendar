//
//  VADayView.swift
//  VACalendar
//
//  Created by Anton Vodolazkyi on 20.02.18.
//  Copyright © 2018 Vodolazkyi. All rights reserved.
//

import UIKit

@objc
public protocol VADayViewAppearanceDelegate: class {
    @objc optional func font(for state: VADayState) -> UIFont
    @objc optional func textColor(for state: VADayState) -> UIColor
    @objc optional func textBackgroundColor(for state: VADayState) -> UIColor
    @objc optional func backgroundColor(for state: VADayState) -> UIColor
    @objc optional func borderWidth(for state: VADayState) -> CGFloat
    @objc optional func borderColor(for state: VADayState) -> UIColor
    @objc optional func dotBottomVerticalOffset(for state: VADayState) -> CGFloat
    @objc optional func shape() -> VADayShape
    // percent of the selected area to be painted
    @objc optional func selectedArea() -> CGFloat
}

protocol VADayViewDelegate: class {
    func dayStateChanged(_ day: VADay)
}

class VADayView: UIView {
    
    var day: VADay
    weak var delegate: VADayViewDelegate?
    
    weak var dayViewAppearanceDelegate: VADayViewAppearanceDelegate? {
        return (superview as? VAWeekView)?.dayViewAppearanceDelegate
    }
    
    private var dotStackView: UIStackView {
        let stack = UIStackView()
        stack.distribution = .fillEqually
        stack.axis = .horizontal
        stack.spacing = dotSpacing
        return stack
    }
    
    private var vStackView: UIStackView {
        let stack = UIStackView()
        stack.distribution = .fillEqually
        stack.axis = .vertical
        stack.spacing = dotSpacing
        return stack
    }
    
    private let dotSpacing: CGFloat = 5
    private let dotSize: CGFloat = 20
    private var supplementaryViews = [UIView]()
    private let dateLabel = UILabel()
    
    init(day: VADay) {
        self.day = day
        super.init(frame: .zero)
        
        self.day.stateChanged = { [weak self] state in
            self?.setState(state)
        }
        
        self.day.supplementariesDidUpdate = { [weak self] in
            self?.updateSupplementaryViews()
        }
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapSelect))
        addGestureRecognizer(tapGesture)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupDay() {
        let shortestSide: CGFloat = (frame.width < frame.height ? frame.width : frame.height)
//        let side: CGFloat = shortestSide * (dayViewAppearanceDelegate?.selectedArea?() ?? 0.8)
        
        dateLabel.font = dayViewAppearanceDelegate?.font?(for: day.state) ?? dateLabel.font
        dateLabel.text = VAFormatters.dayFormatter.string(from: day.date)
        dateLabel.textAlignment = .center
        dateLabel.frame = CGRect(
            x: frame.width - 30,
            y: 0,
            width: 30,
            height: 30
        )
        if day.state == .out {
            self.backgroundColor =  UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
        } else {
            self.backgroundColor =  UIColor.init(red: 237/255, green: 236/255, blue: 253/255, alpha: 1.0)
        }
        
        setState(day.state)
        
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor.init(red: 205/255, green: 203/255, blue: 249/255, alpha: 1).cgColor
        
        addSubview(dateLabel)
        updateSupplementaryViews()
    }
    
    @objc
    private func didTapSelect() {
        guard day.state != .out && day.state != .unavailable else { return }
        delegate?.dayStateChanged(day)
    }
    
    private func setState(_ state: VADayState) {
//        if dayViewAppearanceDelegate?.shape?() == .circle && state == .selected {
//            dateLabel.clipsToBounds = true
////            dateLabel.layer.cornerRadius = dateLabel.frame.height / 2
//        }
        
//        backgroundColor = dayViewAppearanceDelegate?.backgroundColor?(for: state) ?? backgroundColor
//        layer.borderColor = dayViewAppearanceDelegate?.borderColor?(for: state).cgColor ?? layer.borderColor
//        layer.borderWidth = dayViewAppearanceDelegate?.borderWidth?(for: state) ?? dateLabel.layer.borderWidth
        
//        dateLabel.textColor = dayViewAppearanceDelegate?.textColor?(for: state) ?? dateLabel.textColor
//        dateLabel.backgroundColor = dayViewAppearanceDelegate?.textBackgroundColor?(for: state) ?? dateLabel.backgroundColor
        
        updateSupplementaryViews()
    }
    
    private func updateSupplementaryViews() {
        removeAllSupplementaries()
        
        day.supplementaries.forEach { supplementary in
            switch supplementary {
            case .bottomDots(let colors, let numbers):
                let stack = dotStackView
                let stack1 = dotStackView
                let spaceStack = dotStackView
                let vstack = vStackView
                
                if colors.count == 4 {
                    for i in 0..<2 {
                        let color = colors[i]
                        let number = numbers[i]
                        let dotView = VADotView(size: dotSize, color: color, number: number)
                        stack.addArrangedSubview(dotView)
                    }
                    
                    for i in 0..<2 {
                        let color = colors[i+2]
                        let number = numbers[i+2]
                        let dotView = VADotView(size: dotSize, color: color, number: number)
                        stack1.addArrangedSubview(dotView)
                    }
                }
//                else if colors.count == 3 {
//                    for i in 0..<2 {
//                        let color = colors[i]
//                        let number = numbers[i]
//                        let dotView = VADotView(size: dotSize, color: color, number: number)
//                        stack.addArrangedSubview(dotView)
//                    }
//
//                    for i in 0..<1 {
//                        let color = colors[i+2]
//                        let number = numbers[i+2]
//                        let dotView = VADotView(size: dotSize, color: color, number: number)
//                        stack1.addArrangedSubview(dotView)
//
//                        let blank = VADotView(size: dotSize, color: .clear, number: 0)
//                        stack1.addArrangedSubview(blank)
//                    }
//                } else if colors.count == 2 {
//                    for i in 0..<colors.count {
//                        let color = colors[i]
//                        let number = numbers[i]
//                        let dotView = VADotView(size: dotSize, color: color, number: number)
//                        stack.addArrangedSubview(dotView)
//                    }
//                } else if colors.count == 1 {
//                    for i in 0..<colors.count {
//                        let color = colors[i]
//                        let number = numbers[i]
//                        let dotView = VADotView(size: dotSize, color: color, number: number)
//                        stack.addArrangedSubview(dotView)
//
//                        let blank = VADotView(size: dotSize, color: .clear, number: 0)
//                        stack.addArrangedSubview(blank)
//                    }
//                }
                
                let spaceOffset = CGFloat(2 - 1) * dotSpacing
                let stackWidth = CGFloat(2) * dotSize + spaceOffset
                
                let verticalOffset = dayViewAppearanceDelegate?.dotBottomVerticalOffset?(for: day.state) ?? 2
                stack.frame = CGRect(x: 0, y: dateLabel.frame.maxX + verticalOffset, width: stackWidth, height: dotSize)
                stack.center.x = frame.width / 2
                
                spaceStack.frame = CGRect(x: 0, y: dateLabel.frame.maxX + verticalOffset + dotSize, width: stackWidth, height: 5)
                spaceStack.center.x = frame.width / 2
                
                stack1.frame = CGRect(x: 0, y: dateLabel.frame.maxX + verticalOffset + dotSize + 5, width: stackWidth, height: dotSize)
                stack1.center.x = frame.width / 2
                
                vstack.frame = CGRect(x: 0, y: -10, width: stackWidth, height: (dotSize * 2) + 10)
                
                vstack.addSubview(stack)
                vstack.addSubview(spaceStack)
                vstack.addSubview(stack1)
                
                addSubview(vstack)
                supplementaryViews.append(vstack)
            }
        }
    }
    
    private func removeAllSupplementaries() {
        supplementaryViews.forEach { $0.removeFromSuperview() }
        supplementaryViews = []
    }
    
}
