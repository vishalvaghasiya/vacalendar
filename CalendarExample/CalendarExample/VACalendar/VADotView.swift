//
//  VADotView.swift
//  VACalendar
//
//  Created by Anton Vodolazkyi on 25.02.18.
//  Copyright © 2018 Vodolazkyi. All rights reserved.
//

import UIKit

class VADotView: UILabel {
    
    init(size: CGFloat, color: UIColor, number:Int) {
        let frame = CGRect(x: 0, y: 0, width: size, height: size)
        super.init(frame: frame)
        
        layer.cornerRadius = frame.width / 2
        clipsToBounds = true
        backgroundColor = number == 0 ? .clear : color
        text = number == 0 ? "" : "\(number)"
        textColor = number == 0 ? .clear : .white
        font = .boldSystemFont(ofSize: 8)
        textAlignment = .center
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
