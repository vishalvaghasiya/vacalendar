//
//  LongPressViewController.swift
//  JZCalendarWeekViewExample
//
//  Created by Jeff Zhang on 30/4/18.
//  Copyright © 2018 Jeff Zhang. All rights reserved.
//

import UIKit
import JZCalendarWeekView
class LongPressViewController: UIViewController {
    
    @IBOutlet weak var calendarWeekView: JZLongPressWeekView!
    let viewModel = AllDayViewModel()
    
    var curruntWeekDate:Date?
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        setupBasic()
        setupCalendarView()
//        setupNaviBar()
    }
    
    // Support device orientation change
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        JZWeekViewHelper.viewTransitionHandler(to: size, weekView: calendarWeekView)
    }
    
    private func setupCalendarView() {
        calendarWeekView.baseDelegate = self
        
//        if viewModel.currentSelectedData != nil {
//            setupCalendarViewWithSelectedData()
//        } else {
            calendarWeekView.setupCalendar(numOfDays: 7,
                                           setDate: Date(),
                                           allEvents: viewModel.eventsByDate,
                                           scrollType: .pageScroll,
                                           scrollableRange: (nil, nil))
//        }
        
        // LongPress delegate, datasorce and type setup
        calendarWeekView.longPressDelegate = self
        calendarWeekView.longPressDataSource = self
        calendarWeekView.longPressTypes = [.move]//[.addNew, .move]
        
        calendarWeekView.updateFlowLayout(JZWeekViewFlowLayout(columnHeaderHeight: 80, hourGridDivision: .minutes_15))
        
        // Optional
        calendarWeekView.addNewDurationMins = 120
        calendarWeekView.moveTimeMinInterval = 15
    }
    
    /// For example only
    private func setupCalendarViewWithSelectedData() {
        //        guard let selectedData = viewModel.currentSelectedData else { return }
        //        calendarWeekView.setupCalendar(numOfDays: selectedData.numOfDays,
        //                                       setDate: selectedData.date,
        //                                       allEvents: viewModel.eventsByDate,
        //                                       scrollType: selectedData.scrollType,
        //                                       firstDayOfWeek: selectedData.firstDayOfWeek)
        //        calendarWeekView.updateFlowLayout(JZWeekViewFlowLayout(hourGridDivision: selectedData.hourGridDivision))
    }
    
    @IBAction func nextClick(_ sender: UIButton) {
        self.curruntWeekDate = self.curruntWeekDate?.add(component: .day, value: calendarWeekView.numOfDays*2)
        calendarWeekView.updateWeekView(to: self.curruntWeekDate!)
        updateTitle()
    }
    
    @IBAction func prevClick(_ sender: UIButton) {
        self.curruntWeekDate = self.curruntWeekDate?.add(component: .day, value: 0)
        calendarWeekView.updateWeekView(to: self.curruntWeekDate!)
        updateTitle()
    }
}

extension LongPressViewController: JZBaseViewDelegate {
    func initDateDidChange(_ weekView: JZBaseWeekView, initDate: Date) {
        self.curruntWeekDate = initDate
        updateTitle()
    }
}

// LongPress core
extension LongPressViewController: JZLongPressViewDelegate, JZLongPressViewDataSource {
    
    func weekView(_ weekView: JZLongPressWeekView, didEndAddNewLongPressAt startDate: Date) {
        let newEvent = AllDayEvent(id: UUID().uuidString, title: "New Event", startDate: startDate, endDate: startDate.add(component: .hour, value: weekView.addNewDurationMins/120),
                                   location: "Melbourne", isAllDay: false)
        
        if viewModel.eventsByDate[startDate.startOfDay] == nil {
            viewModel.eventsByDate[startDate.startOfDay] = [AllDayEvent]()
        }
        viewModel.events.append(newEvent)
        viewModel.eventsByDate = JZWeekViewHelper.getIntraEventsByDate(originalEvents: viewModel.events)
        weekView.forceReload(reloadEvents: viewModel.eventsByDate)
    }
    
    func weekView(_ weekView: JZLongPressWeekView, editingEvent: JZBaseEvent, didEndMoveLongPressAt startDate: Date) {
        guard let event = editingEvent as? AllDayEvent else { return }
        let duration = Calendar.current.dateComponents([.minute], from: event.startDate, to: event.endDate).minute!
        let selectedIndex = viewModel.events.firstIndex(where: { $0.id == event.id })!
        viewModel.events[selectedIndex].startDate = startDate
        viewModel.events[selectedIndex].endDate = startDate.add(component: .minute, value: duration)
        
        viewModel.eventsByDate = JZWeekViewHelper.getIntraEventsByDate(originalEvents: viewModel.events)
        weekView.forceReload(reloadEvents: viewModel.eventsByDate)
    }
    
    func weekView(_ weekView: JZLongPressWeekView, viewForAddNewLongPressAt startDate: Date) -> UIView {
        if let view = UINib(nibName: EventCell.className, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? EventCell {
            view.titleLabel.text = "New Event"
            return view
        }
        return UIView()
    }
    
    private func updateTitle() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMM"
        
        self.navigationItem.title = dateFormatter.string(from: calendarWeekView.initDate.add(component: .day, value: calendarWeekView.numOfDays)) + " - " + dateFormatter.string(from: calendarWeekView.initDate.add(component: .day, value: calendarWeekView.numOfDays*2 - 1))
    }
}

