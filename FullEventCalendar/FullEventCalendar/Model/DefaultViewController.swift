//
//  DefaultViewController.swift
//  JZCalendarViewExample
//
//  Created by Jeff Zhang on 3/4/18.
//  Copyright © 2018 Jeff Zhang. All rights reserved.
//

import UIKit
import JZCalendarWeekView

class DefaultViewController: UIViewController {

    @IBOutlet weak var calendarWeekView: DefaultWeekView!

    let viewModel = DefaultViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        setupCalendarView()
    }

    // Support device orientation change
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        JZWeekViewHelper.viewTransitionHandler(to: size, weekView: calendarWeekView)
    }

    private func setupCalendarView() {
        calendarWeekView.baseDelegate = self

        // For example only
//        if viewModel.currentSelectedData != nil {
//            setupCalendarViewWithSelectedData()
//            return
//        }
        // Basic setup
        calendarWeekView.setupCalendar(numOfDays: 3,
                                       setDate: Date(),
                                       allEvents: viewModel.eventsByDate,
                                       scrollType: .pageScroll)
        // Optional
        calendarWeekView.updateFlowLayout(JZWeekViewFlowLayout(hourGridDivision: JZHourGridDivision.noneDiv))
    }

    /// For example only
//    private func setupCalendarViewWithSelectedData() {
//        guard let selectedData = viewModel.currentSelectedData else { return }
//        calendarWeekView.setupCalendar(numOfDays: selectedData.numOfDays,
//                                       setDate: selectedData.date,
//                                       allEvents: viewModel.eventsByDate,
//                                       scrollType: selectedData.scrollType,
//                                       firstDayOfWeek: selectedData.firstDayOfWeek)
//        calendarWeekView.updateFlowLayout(JZWeekViewFlowLayout(hourGridDivision: selectedData.hourGridDivision))
//    }
}

extension DefaultViewController: JZBaseViewDelegate {
    func initDateDidChange(_ weekView: JZBaseWeekView, initDate: Date) {
        
    }
}

